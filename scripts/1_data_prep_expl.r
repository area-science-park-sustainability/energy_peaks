# data_prep

peak_threshold <- 1100

library(tidyverse)
options(tidyverse.quiet = TRUE)

library(lubridate)
lubridate.week.start = 7

## set seasons
base_calendar = tibble (dt = seq(ymd_hm('2018-01-01 00:00'),ymd_hm('2022-12-31 23:45'), by = '15 mins')) %>%
    mutate(yd = yday(dt)) %>%
    mutate(season = "winter")%>%
    mutate(season = if_else( (yd>=80 & yd<=171),"spring", season))%>%
    mutate(season = if_else( (yd>=171 & yd<=262),"summer", season))%>%
    mutate(season = if_else( (yd>=262 & yd<=353),"autumn", season))%>%
    select(-yd)
base_calendar %>% write_csv('./data/base_cal.csv')


deg_util = tibble (dt = seq(ymd_hm('2018-01-01 00:00'),ymd_hm('2022-12-31 23:45'), by = '1 day')) %>%
    mutate(yy = year(dt)) %>%
    mutate(mm = month(dt))%>%
    mutate(yd = yday(dt)) %>%
    mutate(day = day(dt))%>%
    mutate(tmp = paste0(yy,"-",mm,"-",day))    %>%
    mutate(wd = wday(tmp, week_start = getOption("lubridate.week.start", 1))) %>%
    summarise(dt,yy,mm,yd,day,wd)%>%
    mutate(season = "winter")%>%
    mutate(season = if_else( (yd>=80 & yd<=171),"spring", season))%>%
    mutate(season = if_else( (yd>=171 & yd<=262),"summer", season))%>%
    mutate(season = if_else( (yd>=262 & yd<=353),"autumn", season))%>%
    mutate(activity = 1)%>%
    mutate(activity = if_else(wd == 5, 2, activity))%>%
    mutate(activity = if_else(wd == 6, 3, activity))%>%
    mutate(activity = if_else(wd == 7, 3, activity))%>%
    mutate(activity = if_else((mm==4)&(day==25), 3, activity))%>%
    mutate(activity = if_else((mm==5)&(day==1), 3, activity))%>%
    mutate(activity = if_else((mm==6)&(day==2), 3, activity))%>%
    mutate(activity = if_else((mm==11)&(day==2), 3, activity))%>%
    mutate(activity = if_else((mm==8)&(day>=14)&(day<=16), 3, activity))%>%  
    mutate(activity = if_else((mm==12)&(day>=24) , 3, activity))%>%  
    mutate(activity = if_else((mm==1)&(day<=6) , 3, activity))%>%  
    mutate(activity = if_else((mm==11)&(day>=1)&(day<=3), 3, activity))%>%
    mutate(activity = if_else((yy==2020)&(mm>=3)&(mm<=5), 3, activity))%>%
    mutate(activity = round(activity,3))%>%
    mutate(daytype = activity)%>%
    mutate(dat=date(dt))%>%
    select(dat, season, mm, wd, daytype)  
deg_util %>% write_csv('./data/deg_util.csv')
table(deg_util$day)
day=date("2022-11-07")
 wday(day, week_start = getOption("lubridate.week.start", 1))

# load data on power consumption
print("loading energy and power data...")
data_foder <- './raw_data/power/'
files <- list.files(path=data_foder,pattern='*.csv')

energy_dataset <- tibble("dd","tt",'energy')%>%head(0)
for (f in files){
    d <- read_delim(paste0(data_foder,f),delim = ";",show_col_types = FALSE)%>% 
        slice(6:nrow(.))%>%
        set_names(nm = "date_time","power","unused")%>%
        mutate(dt =  parse_date_time(date_time,"%d-%m-%y %H:%M"))%>%
        select(-unused, -date_time)%>%
        mutate(year = year(dt)) %>%
        mutate(power = as.numeric(power) / 1000) %>% #power in Kw
        mutate(kWh = power / 4 )%>% # kWh
        relocate(year, dt, kWh, power) %>%
        drop_na()
    energy_dataset <- rbind(energy_dataset,d)
}
head(energy_dataset)

# removing outliers 
print("removing outliers...")
no_outliers <- energy_dataset %>% 
    arrange(dt) %>%
    mutate(day = date(dt))%>%
    group_by(day)%>%
    summarise(min_power = min(power))  %>%
    filter(min_power > 480) %>% 
    select(day)%>% pull()

print(paste("before cleaning", nrow(energy_dataset)))
energy_dataset <- energy_dataset %>% 
    mutate(day = date(dt))%>%
    filter(day %in% no_outliers)%>%
    arrange(dt)%>%
    select(dt,kWh, power)

print(paste("after cleaning", nrow(energy_dataset)))

energy_dataset_hh <- energy_dataset %>% 
    mutate(day = date(dt))%>%
    mutate(hh = hour(dt))%>%
    group_by(day,hh)%>%
    summarise(day, hh, enkWh = sum(kWh), maxpower = max(power))%>%
    distinct()%>%
    mutate(dt = paste0(day,"T", hh,":00:00Z"))%>%
    relocate(dt,day,hh,enkWh,maxpower)%>% 
    mutate(enkWh = round(enkWh,3))%>%
    mutate(maxpower = round(maxpower,3))


 
energy_dataset_hh %>% write_csv( "./data/energy.csv")
colnames(energy_dataset_hh)
head(energy_dataset_hh)

energy_dataset_hh <-  energy_dataset_hh %>% 
                        mutate(dat = day) %>%
                        merge(deg_util, by = "dat") 

energy_dataset_hh %>% filter(wd==6) %>%
    ggplot(aes(x=dt,y=maxpower,group=wd))+geom_line(color="green")

# summarising peaks
print("summarising peaks...")

peaks <- energy_dataset %>% 
    mutate(day = date(dt))%>%
    mutate(quarter_peak = if_else(power > peak_threshold, 1/4, 0)) %>%
    mutate(peak_norm =  power / peak_threshold  ) %>%
    mutate(peak_energy  =  if_else(quarter_peak>0, kWh, 0)) %>%
    group_by(day)%>%
    summarise(energy = sum(kWh), 
                       peak_power = max(power),
                       peak_duration = sum(quarter_peak),
                       peak_energy = round(sum(peak_energy),6))%>%
    mutate(is_peak = if_else(peak_duration >= 1, TRUE, FALSE)) %>%
    filter(!is.na(is_peak))%>%
    mutate(is_peak = as.factor(is_peak))
    


 # load temperature data

print("loading temperature raw data...")
data_foder <- './raw_data/temperature/'
files <- list.files(path=data_foder,pattern='*.csv')
temp_dataset <- tibble("dd","tt", 'temp')%>%head(0)
for (f in files){
    d <- read_delim(paste0(data_foder,f),delim = ";",show_col_types = FALSE)%>% 
            slice(6:nrow(.))%>%
            set_names(nm = "date_time","temp","unused")%>%
            mutate(dt =  parse_date_time(date_time,"%d-%m-%y %H:%M"))%>%
            select(-unused, -date_time)%>%
            relocate(dt,temp)
    temp_dataset <- rbind(temp_dataset,d)
}
temp_dataset <- temp_dataset %>% 
    arrange(dt) %>%
    filter(temp != "-")%>%
    mutate(temp = as.numeric(temp))%>%
    drop_na()%>%
    mutate(hh = hour(dt))%>%
    mutate(day = date(dt))%>%
    relocate(dt, day, hh, temp)

temp_dataset %>% arrange(dt) %>% write_csv( "./data/temperature.csv")

print("extracting night temperatures...")
hours <- seq(1:6)
colnames <- c("day", paste0("T",hours))

night_temperatures <- temp_dataset %>%
    select(day,hh,temp)%>%
    filter(hh %in% hours) %>% 
    pivot_wider(names_from = hh, values_from = temp) %>%
    set_names(nm = colnames)


# full dataset with temperature and peak
data <- merge(night_temperatures,peaks, by="day" ) 
str(data)

# seasons <- base_calendar %>% 
#     mutate(dat=date(dt))%>%
#     mutate(wd=wday(day, week_start = getOption("lubridate.week.start", 2)))%>%
#     select(day,season,wd)

seasons <- deg_util %>% 
    select(dat,season,daytype,wd)%>%
    rename(day = dat)
str(seasons)

data <- data %>% 
    merge(seasons,by = "day") %>%
    distinct()%>%
    relocate(season,daytype)
 
data %>% write_csv( "./data/data.csv")
colnames(data)


print("Done :-D")



