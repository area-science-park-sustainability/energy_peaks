library(tidyverse)
library(mvtnorm)
library(caret)


print("Load data...")
peak_threshold <- 1100

data <- read_csv("./data/data.csv")%>% 
    select(peak_power, is_peak, T6 )%>%
    drop_na()%>%
    arrange(is_peak,T6) %>%
    mutate(is_peak = if_else(peak_power > peak_threshold, TRUE,FALSE)) %>%
    select(-peak_power) %>%
    relocate(is_peak) #must be the first column

data %>% ggplot(aes(x=T6, y=is_peak)) + 
        geom_point(size = 4.0, col = if_else(data$is_peak,"red","green")) + theme_light()
 

colnames(data)
table(data$is_peak)

n = nrow(data)
c = ncol(data)
print("first approach: GLM")
formula <- paste0('is_peak',rawToChar(as.raw(126)),'T6')

model_glm <- glm(formula, data, family=binomial(link="probit"))
print("GLM model coefficients")
print(model_glm$coefficients) 

# LOOCV of GLM model  
print("LOOCV of GLM model  ")
preds_glm <- rep(0, n)
 
for(i in 1:n) {
    print(i)
    test_data <- data[i,]
    train_data <- data[-i,]
    model_glm_i <- glm(formula, train_data, family=binomial(link="probit"))
    preds_glm[i]  <-predict(model_glm_i, test_data, type="response")
}
# loocv_glm <- round(1-mean((data$is_peak - preds_glm)^2),3)
print("LOOCV result for GLM")
actual <- as.factor(data$is_peak)
predicted <- as.factor(if_else(preds_glm > 0.5, TRUE, FALSE))
cm <- confusionMatrix(actual,predicted)#positive = "1")
accuracy_glm <- round((cm$table[1,1]+cm$table[2,2])/sum(cm$table),3)
print("confusion matrix: ")
print(cm$table) 
print(paste("Accuracy: ",accuracy_glm))


data_to_plot <- data 
data_to_plot$pred = preds_glm
data_to_plot$pred_peak = if_else(preds_glm<0.5,FALSE,TRUE)

windows();data_to_plot %>% ggplot() + 
        geom_point(aes(x=T6, y=pred),size = 1.0, col = if_else(data$is_peak,"red","green")) + theme_light()
windows();data_to_plot %>% ggplot() + 
        geom_point(aes(x=T6, y=is_peak),size = 1.0, col = if_else(data$is_peak,"red","green")) + theme_light()




print("second approach: MHMC")
print("Defining functions for MHMC...")
 
post <- function(beta, y=y, X=X){
    pi <- pnorm(X %*% matrix(beta,ncol=1))
    return(prod(pi^y * (1-pi)^(1-y)))
}
 
mh <- function(n_sim, tau, data){
    beta <- matrix(0, nrow=n_sim, ncol=c)
    model <- glm(formula,data,family=binomial(link="probit"))
    sigma.asymp <- summary(model)$cov.unscaled
    beta[1,] <- summary(model)$coefficients[,1]
     
    y <- if_else(data$is_peak, 1,0)
    X <- cbind(1,as.matrix(data[,2:c]))
    
    for(s in 2:n_sim){
        proposal <- rmvnorm(1, beta[s-1,], tau^2*sigma.asymp)
        rho <- min(1, post(proposal, y, X) / post(beta[s-1,], y, X) )
        if(runif(1)<rho){
            beta[s,] <- proposal
        }else{
            beta[s,] <- beta[s-1,]
        }
    }
    return(beta)
}

 



n_sim <- 1e4
tau <- 1.0
burn_in <- n_sim/2
thin <- 1
 # walk...
b <- mh(n_sim, tau, data)
# coefficients
model_mhmc <- model_glm #copy the structure
model_mhmc$coefficients <- c(0,0,0,0,0)
for (i in 1:c){
    model_mhmc$coefficients[i]<- mean(b[burn_in:n_sim,i])
}
print("MHMC model coefficients")
print(model_mhmc$coefficients) 
# 10 fold cross validation of MHMC model  
# preds_mhmc <- rep(0, 4)
accuracy <- c()
model_mhmc_i <- model_mhmc
nfold <- round(n/10,0)
for (j in 1:2){
    print(paste(j, "repetition of 10 fold cross validation"))
    shuffled_idx <- sample(seq(1:n))
    for(k in 1:10) {
        idx <- shuffled_idx %>% head(nfold)
        shuffled_idx <- shuffled_idx %>% tail(length(shuffled_idx)-nfold)
        test_data <- data[idx,]
        train_data <- data[-idx,]
        b <- mh(n_sim, tau, train_data)
        for (i in 1:c) {model_mhmc_i$coefficients[i]<- mean(b[burn_in:n_sim,i])}
        preds_mhmc  <-  predict(model_mhmc_i, test_data, type="response")
        actual <- as.factor(test_data$is_peak)
        predicted <- as.factor(if_else(preds_mhmc > 0.5, TRUE, FALSE))
        cm <- confusionMatrix(actual,predicted)#, positive = "1")
        print(cm$table)
        acc <- round( (cm$table[1,1]+cm$table[2,2])/sum(cm$table), 3)
        accuracy <- c(accuracy, acc)
        print(paste("k fold cross validation - iteration = ", k, "Accuracy = ", acc))
    }
}
title <- paste("Accuracy GLM:", round(accuracy_glm,3), " Accuracy MHMC: ", round(mean(accuracy),3))
to_plot <- tibble(accuracy)%>%mutate(x=1:n())
windows();to_plot %>% ggplot()+
    geom_point(aes(x=x, y=accuracy), col = "red")+
    coord_cartesian (ylim = c( 0.0 , 1.0 )) +
    geom_hline(yintercept = mean(accuracy), col = "red")+
    geom_hline(yintercept = accuracy_glm, col = "#001aff")+
    theme_light()+
    ggtitle(title)

par(mfcol=c(1,3))
## trace plots of i-th variable
ii = 2
plot(b[,ii], type="l", ylab="Trace", xlab="Iteration")
acf(b[,ii], main="")
hist(b[1e3:n_sim,ii], breaks=50, border="gray40",freq=F, main="")
abline(v=mean(b[1e3:n_sim,ii]), col="firebrick3", lty=2)




# Using the selected simulations, give a Monte Carlo estimate of the posterior mean and variance of β (burn-in=1000 and thin=10). 
burn_in = n_sim/2
thin = 1
post_sample <- b[seq(burn_in,n_sim,by=thin),]
#plot
par(mfrow=c(1,c))
for (j in 1:c) plot(post_sample[,j], type="l", ylab="Trace", xlab="Iteration")

par(mfrow=c(1,c))
for (j in 1:c) acf(post_sample[,j], main="")

par(mfrow=c(1,c))
for (j in 1:c){
    hist(post_sample[,j], border="gray40",freq=F, main="")
    abline(v=mean(post_sample[,j]), col="firebrick3", lty=2)
}
# 
print("Posterior mean of Beta")
round(colMeans(post_sample),2)

print("comparison with glm")
model <- glm(formula, data, family=binomial(link="probit"))
round(model$coefficients,2)


 