library(tidyverse)
library(stats)
library(bayesplot)
theme_set(bayesplot::theme_default())

library(rstan)
rstan_options(auto_write = FALSE)
complie_STAN_model = FALSE

print("Load data...")
peak_threshold <- 1100
data <- read_csv("./data/data.csv")%>% 
    select(peak_power, T1,T2,T3,T4,T5,T6,daytype )%>%
    mutate(T = (T1+T2+T3+T4+T5+T6)/6) %>% #average night temperature from 1AM to 6AM
    mutate(intercept = 1)%>%
    mutate(peak = if_else(peak_power > peak_threshold, 1,0)) %>%
    relocate(peak, intercept,T,daytype) %>%
    drop_na()%>%
    arrange(T)


summary(data)

# visualize data
windows(); ggplot(data, aes(x = T, y = as.factor(peak), color=as.factor(peak))) + geom_jitter()

 


stan_data <- list(
  N = nrow(data),       # number of observations
  J = as.integer(3),    # number of groups (MTWT,FRI,WEH)
  K = as.integer(2),    # number of columns in the model matrix (data$T)
  id = data$daytype,
  X  = data %>% select(intercept,T) %>% as.vector(),
  y  = data$peak  
)
str(stan_data)

model_filename = 'model_h'

if ( (file.exists(paste0(model_filename,'.rds'))) & (complie_STAN_model == FALSE) ) {
    print("loading STAN model...")
    compiled_model <- readRDS(paste0(model_filename,'.rds'))
} else {
    print("compiling STAN model...")
    compiled_model <- stan_model(paste0(model_filename,'.stan'))
    saveRDS(compiled_model, file = paste0(model_filename,'.rds'))
    print("STAN model compiled and saved...")
}

print("STAN sampling...")
fit <- sampling(compiled_model, data = stan_data)

print("Results...")
print(fit,pars=c("beta"))

## extract alpha and beta 
exfit <- extract(fit, permuted = TRUE)  
str(exfit)
beta <- exfit$beta
yrep = exfit$y_rep 

results<- tibble(index = 1:4000, 
        bMTWT_i = beta[,1,1],bMTWT = beta[,1,2], 
        bFRI_i  = beta[,2,1],bFRI  = beta[,2,2],
        bWE_i   = beta[,3,1],bWE   = beta[,3,2]) 
results_long <- results %>% pivot_longer(cols=-index,names_to = "param", values_to = "value") 

str(results)
results %>% head()
 
#histograms
results_long %>% ggplot()+aes(x=value)+
  geom_histogram(fill="blue",color="white")+
  facet_wrap(~param, ncol=2, scales = "free")+
  theme_light()

#traces
results_long %>% ggplot()+aes(x=index,y=value)+
  geom_line()+
  facet_wrap(~param, ncol=2, scales = "free")+
  theme_light()


## posterior predictive checking
y_rep <- as.matrix(fit, pars = "y_rep")
windows();ppc_dens_overlay(y = stan_data$y, y_rep[1:400,])

# standardised residuals of the observed vs predicted 
mean_y_rep <- colMeans(y_rep)
std_resid <- (stan_data$y - mean_y_rep) / sqrt(mean_y_rep)
windows();qplot(mean_y_rep, std_resid) + hline_at(2) + hline_at(-2)


# variation in the regression coefficients between the groups  
coeff_quant <- apply(exfit$beta,c(2,3),quantile,probs=c(0.025,0.5,0.975))
df_coeff <- data.frame(Coeff=rep(c("Intercept","beta1"),each=3),LI=c(coeff_quant[1,,1],coeff_quant[1,,2]),Median=c(coeff_quant[2,,1],coeff_quant[2,,2]),HI=c(coeff_quant[3,,1],coeff_quant[3,,2]))
df_coeff$Group<-as.factor(c("1 Mo-Tu-We-Th","2 Friday","3 weekend"))
windows();ggplot(df_coeff,aes(x=Group,y=Median))+geom_point()+
  geom_linerange(aes(ymin=LI,ymax=HI))+coord_flip()+
  facet_grid(.~Coeff,scale="free")+
  labs(y="Regression parameters")+theme_bw()
ggsave(paste0("Figure.png"))

df_coeff <- df_coeff %>% 
  select('Group','Coeff','Median') %>%
  relocate('Group','Coeff','Median')  

coeffs <- df_coeff %>% 
  pivot_wider(names_from = "Coeff", values_from = "Median")
coeffs %>% write_csv("reg_coeff.csv")

print(coeffs)

ee <- tibble ( temperature = seq(from = -10, to = 30, by = .5))
ee$p_MTWT = 1/ (1+exp(-(coeffs$Intercept[1] + ee$temperature * coeffs$beta1[1])))
ee$p_F =    1/ (1+exp(-(coeffs$Intercept[2] + ee$temperature * coeffs$beta1[2])))
ee$p_WE =   1/ (1+exp(-(coeffs$Intercept[3] + ee$temperature * coeffs$beta1[3])))
ee %>% write_csv("ee.csv")

for (i in 1:3){
    print(i)
    a <- coeffs$Intercept[i]
    b <- coeffs$beta1[i]
    res <- data %>% filter(daytype==i) %>%
      mutate(pred_prob = 1/ (1+exp(-(a + T * b))))%>%
      mutate(pred_peak = if_else(pred_prob > .5,1,0))
    
    windows();ggplot() + theme_minimal() + 
      geom_line(data = ee, aes(x=temperature, y=p_MTWT),color="blue")+
      geom_line(data = ee, aes(x=temperature, y=p_F   ),color="black")+
      geom_line(data = ee, aes(x=temperature, y=p_WE  ),color="green")+
      geom_hline(yintercept = 0)+geom_hline(yintercept = 1)  +
      geom_point(data = res, aes(x=T, y=pred_prob, color=as.factor(peak)), alpha=.6,size = 2)
    ggsave(paste0("Figure",i,".png"))
}

 
