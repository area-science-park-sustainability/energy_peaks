data {
  int<lower=1> N;
  int y[N];
  vector [N]X;
  real scale_alpha;
  real scale_beta;
}
parameters {
  real alpha;
  real beta;
}
transformed parameters {
  vector[N] eta;
  eta = alpha + X * beta;
}
model { 
  // weakly informative priors
  alpha ~ normal(0., scale_alpha);
  beta ~ normal(0., scale_beta);
  y ~ bernoulli_logit(eta);
} 
generated quantities {
  // simulate data from the posterior
  real y_rep[N];
  for (i in 1:N) {
    y_rep[i] = bernoulli_rng(inv_logit(eta[i]));
  }
}

 