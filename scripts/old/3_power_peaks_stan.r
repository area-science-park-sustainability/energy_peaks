## work in progress

## example from: https://jrnold.github.io/bayesian_notes/binomial-models.html

## general: https://skeptric.com/getting-started-rstan/
## extract: https://mc-stan.org/rstan/reference/stanfit-method-extract.html 

## saving and plotting https://mc-stan.org/rstan/articles/rstan.html 

#https://mc-stan.org/docs/2_28/stan-users-guide/logistic-probit-regression.html
#https://www.econometrics-with-r.org/11-2-palr.html

library(tidyverse)
library(stats)
library(bayesplot)
theme_set(bayesplot::theme_default())

library(rstan)
rstan_options(auto_write = FALSE)
complie_STAN_model = FALSE

print("Load data...")
peak_threshold <- 1100
data <- read_csv("./data/data.csv")%>% 
    select(peak_power, T1,T2,T3,T4,T5,T6 )%>%
    mutate(T = (T1+T2+T3+T4+T5+T6)/6) %>% #average night temperature from 1AM to 6AM
    mutate(peak = if_else(peak_power > peak_threshold, 1,0)) %>%
    relocate(peak) %>%  
    select(peak, T) %>%
    drop_na()%>%
    arrange(T,peak)

n = nrow(data)
c = ncol(data)
print(paste(n,c))
print(table(data$peak))
summary(data)

# visualize data
windows(); ggplot(data, aes(x = T, y = as.factor(peak), color=as.factor(peak))) + geom_jitter()


stan_data <- list(
  N = nrow(data), 
  X = data$T,
  y = data$peak,
  scale_alpha = 10,
  scale_beta = 10
)
str(stan_data)

if ( (file.exists('model_bernoulli.rds')) & (complie_STAN_model == FALSE) ) {
  print("loading STAN model...")
  compiled_model <- readRDS('model_bernoulli.rds')
} else {
  print("compiling STAN model...")
  compiled_model <- stan_model('model_bernoulli.stan' , save_dso = TRUE)
}

print("STAN sampling...")
fit <- sampling(compiled_model, data = stan_data)

print("Results...")
print(fit, pars = c('alpha','beta'))

## extract alpha and beta 
exfit <- extract(fit, permuted = TRUE)  
results<- tibble(a = exfit$alpha, b=exfit$beta) 
results_long <- results %>% pivot_longer(cols=c("a","b"),names_to = "param", values_to = "value")
str(results)
results %>% head()

yrep <- tibble(yr = exfit$y_rep)
str(yrep)
 
results_long %>% ggplot()+aes(x=value)+
  geom_histogram(fill="blue",color="white")+
  facet_wrap(~param,scales = "free")+
  theme_light()
 

windows();mcmc_hist(as.matrix(fit, pars = c('alpha','beta')))

windows();mcmc_scatter(as.matrix(fit, pars = c('alpha','beta')), alpha = 0.2)

## posterior predictive checking
y_rep <- as.matrix(fit, pars = "y_rep")
windows();ppc_dens_overlay(y = stan_data$y, y_rep[1:200,])

# standardised residuals of the observed vs predicted 
mean_y_rep <- colMeans(y_rep)
std_resid <- (stan_data$y - mean_y_rep) / sqrt(mean_y_rep)
windows();qplot(mean_y_rep, std_resid) + hline_at(2) + hline_at(-2)


pairs(fit, pars = c("alpha", "beta", "lp__"), las = 1)


res <- data %>%
  mutate(pred_prob = 1/ (1+exp(-(a + T * b))))%>%
  mutate(pred_peak = if_else(pred_prob > .5,1,0))


windows();ggplot() + theme_minimal() + 
  geom_line(data = ee, aes(x=t, y=p),color="black")+
  geom_hline(yintercept = 0)+geom_hline(yintercept = 1)+
  geom_point(data = res, aes(x=T, y=pred_peak,color=pred_peak), alpha=.1,size = 5)


windows();ggplot() + theme_minimal() + 
  geom_line(data = ee, aes(x=t, y=p),color="black")+
  geom_hline(yintercept = 0)+geom_hline(yintercept = 1)+
  geom_point(data = res, aes(x=T, y=pred_prob,color=pred_peak), alpha=.1,size = 3)

windows();ggplot() + theme_minimal() + 
  geom_line(data = ee, aes(x=t, y=p),color="black")+
  geom_hline(yintercept = 0)+geom_hline(yintercept = 1)+
  geom_point(data = res, aes(x=T, y=pred_prob,color=peak), alpha=.1,size = 3) 

res %>% write_csv('tmp.csv')
 

# visualize data
windows(); ggplot(res, aes(x = T, y = as.factor(peak), color=as.factor(pred_peak))) + geom_jitter()
