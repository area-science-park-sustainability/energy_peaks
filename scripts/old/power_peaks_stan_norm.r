library(tidyverse)
library(rstan)
library(bayesplot)
theme_set(bayesplot::theme_default())
library(caret)


print("Load data...")
peak_threshold <- 1100
data <- read_csv("./data/data.csv")%>% 
    select(peak_energy, T1,T2,T3,T4,T5,T6, wd )%>%
    mutate(T = (180-T1-T2-T3-T4-T5-T6)/6) %>% #average of (20-Ti)
    rename(peak = peak_energy)%>%
    relocate(peak) %>% #peak must be the first column 
    select(peak, T) %>%
    drop_na()

head(data)
table(data$peak)
n = nrow(data)
c = ncol(data)
print(paste(n,c))
 
# visualize data
windows();ggplot(data, aes(x = T)) + geom_histogram(color = "white", fill = "#0fbd0f")
windows();ggplot(data, aes(x = peak)) + geom_histogram(color = "white", fill = "#0fbd0f")
windows();ggplot(data, aes(x = T, y = peak)) + geom_point(size = 6, alpha=.5, col = "red")
stan_data <- list(
  N = nrow(data), 
  peak = data$peak,
  T = data$T
)
str(stan_data)

print("STAN MODEL...")
model = stan_model("simple_normal.stan")
print("model done")
fit = sampling(model, data = stan_data)
print("fit done")
print(fit, pars = c('alpha','beta'))
windows();mcmc_hist(as.matrix(fit, pars = c('alpha','beta')))
windows();mcmc_scatter(as.matrix(fit, pars = c('alpha','beta')), alpha = 0.2)

## posterior predictive checking
y_rep <- as.matrix(fit , pars = "y_rep")
windows();ppc_dens_overlay(y = stan_data$peak, y_rep[1:00,])

plot(stan_data$peak)

# standardised residuals of the observed vs predicted number of complaints
mean_y_rep <- colMeans(y_rep)
std_resid <- (stan_data$peak - mean_y_rep) / sqrt(mean_y_rep)
qplot(mean_y_rep, std_resid) + hline_at(2) + hline_at(-2)


#prediction
samps <- rstan::extract(fit)
a<-mean(samps$alpha)
b<-mean(samps$beta)
print(paste(a,b))
for (t in seq(-10,30)){
  NT <- 30-t
  pp <- a + b*NT
  print(paste(t, pp))
}

## complare with GLM
 
formula <- paste0('peak',rawToChar(as.raw(126)),'T')
model_glm <- glm(formula, data, family=gaussian(link = "identity"))
print("GLM model coefficients")
print(model_glm$coefficients) 


# LOOCV of GLM model  
print("LOOCV of GLM model  ")
preds_glm <- rep(0, n)
for(i in 1:n) {
    print(i)
    test_data <- data[i,]
    train_data <- data[-i,]
    model_glm_i <- glm(formula, data, family=gaussian(link = "identity"))
    preds_glm[i]  <-predict(model_glm_i, test_data, type="response")
}
std_resid <- (stan_data$peak - preds_glm) / sqrt(preds_glm)
qplot(preds_glm, std_resid) + hline_at(2) + hline_at(-2)
 
