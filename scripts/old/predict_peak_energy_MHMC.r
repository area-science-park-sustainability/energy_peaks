# linear prediction is NOT appropriate for "peak energy"

library(tidyverse)
library(mvtnorm)
library(caret)

print("Load data...")
peak_threshold <- 1100
data <- read_csv("./data/data.csv")%>% 
    select(peak_energy, T1,T2,T3,T4,T5,T6, season,wd )%>%
    filter(season=="summer")%>%
    mutate(T = (180-T1-T2-T3-T4-T5-T6)/6) %>% #average of (20-Ti)
    mutate(peak = peak_energy/1000)%>%
    relocate(peak) %>% #peak must be the first column 
    select(peak, T) %>%
    drop_na()%>%
    arrange(T)



colnames(data)
 
n = nrow(data)
c = ncol(data)
print("first approach: GLM")
formula <- paste0('peak',rawToChar(as.raw(126)),'T')

model_glm <- glm(formula, data, family=gaussian(link = "identity"))
print("GLM model coefficients")
print(model_glm$coefficients) 

# LOOCV of GLM model  
print("LOOCV of GLM model  ")
preds_glm <- rep(0, n)
 
for(i in 1:n) {
    print(i)
    test_data <- data[i,]
    train_data <- data[-i,]
    model_glm_i <- glm(formula, data, family=gaussian(link = "identity"))
    preds_glm[i]  <-predict(model_glm_i, test_data, type="response")
}
std_resid <- (data$peak - preds_glm) / sqrt(preds_glm)
qplot(preds_glm, std_resid) 

data_to_plot <- data 
data_to_plot$pred = preds_glm
colnames(data_to_plot)

windows();data_to_plot %>% ggplot() + 
        geom_point(aes(x=peak, y=pred),size = 1.0) + theme_light() 

print("second approach: MHMC")
print("Defining functions for MHMC...")
 
post <- function(beta, y=y, X=X){
    pi <- pnorm(X %*% matrix(beta,ncol=1))
    return(prod(pi^y * (1-pi)^(1-y)))
}
 
mh <- function(n_sim, tau, data){
    beta <- matrix(0, nrow=n_sim, ncol=c)
    model <- glm(formula, data, family=gaussian(link = "identity"))
    sigma.asymp <- summary(model)$cov.unscaled
    beta[1,] <- summary(model)$coefficients[,1]
     
    y <- data$peak
    X <- cbind(1,as.matrix(data[,2:c]))
    
    for(s in 2:n_sim){
        proposal <- rmvnorm(1, beta[s-1,], tau^2*sigma.asymp)
        rho <- min(1, post(proposal, y, X) / post(beta[s-1,], y, X) )
        if(is.na(rho)){rho=1}
        if(runif(1)<rho){
            beta[s,] <- proposal
        }else{
            beta[s,] <- beta[s-1,]
        }
        print(beta[s,])
    }
    return(beta)
}

n_sim <- 1e3
tau <- 1
burn_in <- n_sim/2
thin <- 1
 # walk...
b <- mh(n_sim, tau, data)
# coefficients
model_mhmc_coefficients <- c(0,0,0,0,0)
for (i in 1:c){
    model_mhmc_coefficients[i]<- mean(b[burn_in:n_sim,i])
}
print("MHMC model coefficients")
print(model_mhmc_coefficients) 
 
par(mfcol=c(1,3))
ii = 2
plot(b[,ii], type="l", ylab="Trace", xlab="Iteration")
acf(b[,ii], main="")
hist(b[1e3:n_sim,ii], breaks=50, border="gray40",freq=F, main="")
abline(v=mean(b[1e3:n_sim,ii]), col="firebrick3", lty=2)

plot(data$peak)


# Using the selected simulations, give a Monte Carlo estimate of the posterior mean and variance of β (burn-in=1000 and thin=10). 
burn_in = n_sim/2
thin = 1
post_sample <- b[seq(burn_in,n_sim,by=thin),]
#plot
par(mfrow=c(1,c))
for (j in 1:c) plot(post_sample[,j], type="l", ylab="Trace", xlab="Iteration")

par(mfrow=c(1,c))
for (j in 1:c) acf(post_sample[,j], main="")

par(mfrow=c(1,c))
for (j in 1:c){
    hist(post_sample[,j], border="gray40",freq=F, main="")
    abline(v=mean(post_sample[,j]), col="firebrick3", lty=2)
}
# 
print("Posterior mean of Beta")
round(colMeans(post_sample),2)

print("comparison with glm")
model <- glm(formula, data, family=binomial(link="probit"))
round(model$coefficients,2)


 