library(tidyverse)
library(rstan)
library(bayesplot)

theme_set(bayesplot::theme_default())

print("Load data...")

print("Load data...")
peak_threshold <- 1100
data <- read_csv("./data/data.csv")%>% 
    select(peak_power, peak_duration, T1,T2,T3,T4,T5,T6, day, wd )%>%
    mutate(T = (180-T1-T2-T3-T4-T5-T6)/6) %>% #average of (20-Ti)
    mutate(peak = if_else(peak_power > peak_threshold, 1,0)) %>%
    relocate(peak) %>% #peak must be the first column 
    select(peak, T) %>%
    drop_na()

n = nrow(data)
c = ncol(data)
print(paste(n,c))
print(table(data$peak))
summary(data)

# visualize data
windows(); ggplot(data, aes(x = T, y = peak)) + geom_jitter()
ggplot(data, aes(x = T, y = peak)) + geom_point(size = 3, alpha=.5)


stan_dat_simple <- list(
  N = nrow(data), 
  peak = data$peak,
  T = data$T
)
str(stan_dat_simple)

comp_model_P <- stan_model('simple_poisson_regression.stan')

fit_P_real_data <- sampling(comp_model_P, data = stan_dat_simple)

print(fit_P_real_data, pars = c('alpha','beta'))

mcmc_hist(as.matrix(fit_P_real_data, pars = c('alpha','beta')))
mcmc_scatter(as.matrix(fit_P_real_data, pars = c('alpha','beta')), alpha = 0.2)

## posterior predictive checking
y_rep <- as.matrix(fit_P_real_data, pars = "y_rep")
windows();ppc_dens_overlay(y = stan_dat_simple$peak, y_rep[1:200,])



# standardised residuals of the observed vs predicted number of complaints
mean_y_rep <- colMeans(y_rep)
std_resid <- (stan_dat_simple$peak - mean_y_rep) / sqrt(mean_y_rep)
qplot(mean_y_rep, std_resid) + hline_at(2) + hline_at(-2)





