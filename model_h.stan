data {
	int<lower=1> N; 			// number of observations
	int<lower=1> J; 			// number of groups
	int<lower=1> K; 			// number of columns in the model matrix
	int<lower=1,upper=J> id[N]; //vector of group indeces
	matrix[N,K] X; 				//the model matrix
	int y[N];					//the response variable
 				
}
parameters {
	vector[K] beta[J]; 			//matrix of group-level regression coefficients
}
transformed parameters {
  	vector[N] eta;
	for(i in 1:N){
		eta[i] = X[i] * beta[id[i]]; 
	}
}
model { 
	for(j in 1:J){
		beta[j] ~ normal(0., 5); 
	}
	
 	for(i in 1:N){
		y[i] ~ bernoulli_logit(eta[i]);
	}

	
} 
generated quantities {
  // simulate data from the posterior
  real y_rep[N];
  for (i in 1:N) {
    y_rep[i] = bernoulli_rng(inv_logit(eta[i]));
  }
}

 