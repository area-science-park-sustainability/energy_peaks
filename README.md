# Early detection of peaks in energy consumption due summer to air conditioning. 

## Authors and acknowledgment
project developed by Fabio Morea, Area Science Park (Trieste, Italy [https://www.areasciencepark.it]). Contact: fabio.morea@areasciencepark.it

The project is developed in the frame of the PhD program in Applied Data Science and Artificial Intelligence at University of Trieste ([https://adsai.units.it/]).

## Description
Electricity consumption peaks are a major concern for building owners, especially on hot summer days, as they generate high costs, cause transformers to overheat and can even reach the maximum available power with a consequent risk of blackout.

Several effective "peak-shaving" strategies can be put in place, such as a higher temperature setpoint (which implees a temporary reduction of comfort levels), switching off low-priority processes, and starting the cooling process earlier than usual. 

Currently the above strategies may put in place starting at 8:00, based on observed temperature and  the personal judgment of an expert.

All the strategies mentioned would be more effective if put in place earlier (e.g. start cooling at 6:00, notify users the reduction in temperature setpoint by 7:00...).

The objective of this project is to create an early detection system that can assess the probability of an energy peak during the day, based on information available before 6:00 ( such as temperatures during the night) and an estimated level of activity of the followong day (depending on weekday, month and holiday) 

We evaluate the probability of an energy peak using the following models: 

* Logit - Generalized Linear Model (R)
* Logit - (R + STAN)

Models are compared by **accuracy of predictions** estimated with LOOCV (or k-fold cross validation for slow algorithms). 

## License
SPDX-License-Identifier: CC-BY-4.0

## Project status
This is the beta version, developed in November 2021.
