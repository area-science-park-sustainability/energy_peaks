data {
  int<lower=1> N;
  int<lower=0> y[N];
  vector [N]T;
}
parameters {
  real alpha;
  real beta;
}
transformed parameters {
  vector[N] eta;
  eta = alpha + T * beta;
}
model { 
  // weakly informative priors
  alpha ~ normal(0, 3);
  beta ~ normal(0, 3);
  y ~ bernoulli_logit(alpha + beta * T);
} 
generated quantities {
  // simulate data from the posterior
  vector[N] y_rep;

  for (i in 1:num_elements(y_rep)) {
    y_rep[i] = bernoulli_rng(inv_logit(eta[i]));
  }
}

 