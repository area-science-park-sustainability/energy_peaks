functions {
  /*
  * Alternative to poisson_log_rng() that  avoids potential numerical problems during warmup
  */
  int poisson_log_safe_rng(real eta) {
    real pois_rate = exp(eta);
    if (pois_rate >= exp(20.79))
      return -9;
    return poisson_rng(pois_rate);
  }
}

data {
  int<lower=1> N;
  int<lower=0> peak[N];
  vector<lower=0> [N]T;
}
parameters {
  real alpha;
  real beta;
}
model { 
  // weakly informative priors:
  // we expect negative slope on T and a positive intercept,
  // but we will allow ourselves to be wrong
  beta ~ normal(-0.25, 1);
  alpha ~ normal(log(4), 1);
  
  // poisson_log(eta) is more efficient and stable alternative to poisson(exp(eta))
  peak ~ poisson_log(alpha + beta * T);
} 

generated quantities {
  // sample predicted values from the model for posterior predictive checks
  int y_rep[N];

  // for each obs we simulate a new value of y using the simulated parameters
  // the result will be a matrix with nrow = iter and ncol = n
  for (n in 1:N) {
    real eta_n = alpha + beta * T[n];
    y_rep[n] = poisson_log_safe_rng(eta_n);
  }
}
