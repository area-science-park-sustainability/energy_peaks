
/*
*Simple normal regression  
*/

data {
  int<lower=1> N;
  real<lower=0> peak[N];
  real<lower=0> T[N];
}
parameters {
  real alpha;
  real beta;
  real <lower=0> sigma;
}
model {
  // weakly informative priors
  alpha ~ normal(0, 1);
  beta ~ normal(0, 1);
  sigma ~ cauchy(0,2);
  for (i in 1:N)
    peak[i] ~ normal(alpha + beta* T[i], sigma);
} 


generated quantities {
  // sample predicted values from the model for posterior predictive checks
  real y_rep[N];

  // for each obs we simulate a new value of y using the simulated parameters
  // the result will be a matrix with nrow = iter and ncol = n
  for (i in 1:N) {
    y_rep[i] = normal_rng(alpha + beta * T[i], sigma);
  }
}
